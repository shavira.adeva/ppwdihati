from django.db import models
from fitur_profile.models import Pengguna

class MessageKu(models.Model):
    pengguna = models.ForeignKey(Pengguna,on_delete=models.CASCADE)
    message = models.TextField()
    created_date = models.DateTimeField(auto_now_add=True)