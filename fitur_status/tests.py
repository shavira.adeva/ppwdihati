from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from fitur_profile.models import MessageKu, Pengguna
from .forms import Message_Form
from .views import index, message_post, get_data_session,get_data_user, create_new_user, set_data_for_session

# Create your tests here.

class StatusUnitTest(TestCase):
    def setUp(self):
        self.username = 'shavira.adeva'
        self.password = 'adesacucmey98'

    def test_update_status_url_is_exist(self):
        response = Client().get('/status/'+self.username)
        self.assertEqual(response.status_code, 301)