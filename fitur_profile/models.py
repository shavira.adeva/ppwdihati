from django.db import models
# Create your models here.

class Pengguna(models.Model):
    kode_identitas = models.CharField('Kode Identitas', max_length=20, primary_key=True,)
    nama = models.CharField('Nama', max_length=200)
    email = models.CharField('email',max_length = 50)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

class KeahlianKu(models.Model):
    dependencies = [
        ('ook', '__first__'),
        ('eek', '0002_auto_20151029_1040'),
    ]
    
    pengguna = models.ForeignKey(Pengguna, null=True)
    keahlian = models.CharField("Kode keahlian", max_length=50)
    level = models.CharField(max_length = 20)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
