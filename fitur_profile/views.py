from django.shortcuts import render
from .models import  KeahlianKu
from .utils import *
from django.http import HttpResponseRedirect
from django.urls import reverse
# Create your views here.
response = {}
def index(request):
	landing_page = 'profile.html'
	response['project'] = "Tugas 2"
	set_data_for_session(request)
	# response['author'] = request.session['kode_identitas']
	response['list_keahlian'] = get_keahlian_dari_yang_login(request)
	return render(request,landing_page,response)

def edit_profile(request):
	if 'user_login' in request.session:
		html = 'edit_profile.html'
		return render(request,html,response)
	else:
		return HttpResponseRedirect(reverse())
### SESSION : GET and SET
def get_data_session(request):
    if get_data_user(request, 'user_login'):
        response['author'] = get_data_user(request, 'user_login')

def set_data_for_session(request):
    response['author'] = get_data_user(request, 'user_login')
    response['kode_identitas'] = request.session['kode_identitas']
    response['role'] = request.session['role']

def add_keahlian_to_database(request,id_skill,id_level):
	skills = ['Java','C#','Python','Erlang','Kotlin']
	levels = ['Beginner','Intermediate','Advanced','Expert','Legend']
	response['kode_identitas'] = request.session['kode_identitas']
	npm = response['kode_identitas']

	pengguna_sekarang = Pengguna.objects.get(kode_identitas = npm)

	skill_taken = skills[int(id_skill)]
	level_of_skill = levels[int(id_level)]
	
	objek_keahlian = KeahlianKu()
	objek_keahlian.pengguna = pengguna_sekarang
	objek_keahlian.keahlian = skill_taken
	objek_keahlian.level = level_of_skill

	objek_keahlian.save()

	return HttpResponseRedirect(reverse('profile:edit_profile'))

def delete_keahlian_from_database(request,id_delete):
	objek_akan_didelete = KeahlianKu.objects.get(id = id_delete)

	objek_akan_didelete.delete()

	return HttpResponseRedirect(reverse('profile:edit_profile'))
	

def get_keahlian_dari_yang_login(request):
	response['kode_identitas'] = request.session['kode_identitas']
	res = []
	pengguna_sekarang = Pengguna.objects.get(kode_identitas = response['kode_identitas'])
	keahlian_pengguna = KeahlianKu.objects.filter(pengguna = pengguna_sekarang)

	for keahlian in keahlian_pengguna:
		res.append(keahlian)

	return res

def add_to_pengguna_database(request,firstName,lastName,imageUrl,email,profileUrl):
	nama = firstName + " " + lastName
	gambar = imageUrl
	email = email
	linkedIn_link = profileUrl

	response['kode_identitas'] = request.session['kode_identitas']
	identitas = response['kode_identitas']

	pengguna_sekarang = Pengguna.objects.get(kode_identitas = identitas)

	pengguna_sekarang.nama = nama
	pengguna.email = email

	response['image'] = imageUrl
	response['nama'] = nama
	response['email'] = email
	response['keahlian'] = get_keahlian_dari_yang_login(request)
	response['linkedIn_link'] = profileUrl
	
def model_to_dict(obj):
    data = serializers.serialize('json', [obj,])
    struct = json.loads(data)
    data = json.dumps(struct[0]["fields"])
    return data
