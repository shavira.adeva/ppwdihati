from django.apps import AppConfig


class FiturProfileConfig(AppConfig):
    name = 'fitur_profile'
