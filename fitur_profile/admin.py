from django.contrib import admin
from .models import Pengguna, KeahlianKu
# Register your models here.
admin.site.register(Pengguna)
admin.site.register(KeahlianKu)