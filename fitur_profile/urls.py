from django.conf.urls import url
from .views import index, edit_profile, add_keahlian_to_database, delete_keahlian_from_database,add_to_pengguna_database

#url for profile

urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^edit_profile/$', edit_profile, name='edit_profile'),
    url(r'^edit_profile/add_keahlian/(?P<id_skill>[0-9]+)/(?P<id_level>[0-9]+)/$'
    	, add_keahlian_to_database , name='add_keahlian'),
    url(r'^edit_profile/delete_keahlian/(?P<id_delete>[0-9]+)/$', delete_keahlian_from_database, name='delete_keahlian'),
    url(r'^edit_profile/add_to_database/',add_to_pengguna_database,name='add_to_database')
]	