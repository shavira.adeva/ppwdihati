from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .api_riwayat import get_matakuliah

import environ

root = environ.Path(__file__) - 3 # three folder back (/a/b/c/ - 3 = /)
env = environ.Env(DEBUG=(bool, False),)
environ.Env.read_env('.env')

# Create your tests here.
class FiturRiwayatUnitTest(TestCase):

	def test_matakuliah_url_is_exist(self):
		response = Client().get('/riwayat/')
		self.assertEqual(response.status_code, 200)

	def test_matakuliah_using_index_func(self):
		found = resolve('/riwayat/')
		self.assertEqual(found.func, index)
