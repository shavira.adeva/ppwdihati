from django.conf.urls import url
from .views import index, matakuliah

urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^matakuliah/$', matakuliah, name='matakuliah'),

]
