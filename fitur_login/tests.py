from django.test import TestCase
from django.test import Client
from django.urls import resolve

# Create your tests here.
class LoginUnitTest(TestCase):

    def setUp(self):
        self.username = 'shavira.adeva'
        self.password = 'adesacucmey98'

    def test_masuk_ke_login_tanpa_ke_awal(self):
        response = self.client.get('/login/index')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed('login/SSO UI.html')

    def test_masuk_ke_awal(self):
        response = self.client.get('/login/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed('login/awal.html')

    def test_auth_logout(self):
        login = self.client.post('/login/custom_auth/login/', {"username": self.username, "password": self.password})
        logout = self.client.get('/login/custom_auth/logout/')
        self.assertEqual(logout.status_code, 302)
        self.assertRaisesMessage("Anda berhasil logout. Semua session Anda sudah dihapus", logout)

    def test_login_url_is_exist(self):
        response = self.client.get('/login/')
        self.assertEqual(response.status_code, 200)

    def test_login_page_when_user_is_logged_in_and_otherwise(self):
        #not logged in, render login template
        response = self.client.get('/login/index')
        html_response = response.content.decode('utf-8')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed('login/SSO UI.html')
        #logged in, redirect to dashboard
        self.client.post('/login/custom_auth/login/', {'username': self.username, 'password': self.password})
        response = self.client.get('/status/'+self.username)
        self.assertEqual(response.status_code, 301)
        self.assertTemplateUsed('status/update_status.html')


# Create your tests here.
