from django.shortcuts import render
from fitur_profile.models import Pengguna, KeahlianKu
import json

res = {}

def index(request):
    res['key'] = ''
    html = 'search/search.html'
    res['data'] = get_user_data()
    return render(request, html, res)

def search(request, key):
    res['key'] = key
    html = 'search_mahasiswa/search.html'
    res['data'] = get_user_data()
    return render(request, html, res)

def get_user_data():
    users = Pengguna.objects.all()
    data = []
    for user in users:
        temp = {}
        temp['username'] = user.nama
        temp['npm'] = user.npm
        temp['email'] = user.email
        exps = Keahlianku.objects.filter(pengguna=user)
        temp['expertise'] = exps
        data.append(temp)
    return json.dumps(data)
